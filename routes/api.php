<?php

use App\Http\Controllers\ApiController;
use App\Http\Controllers\SwCardGameController;
use App\Http\Controllers\SwScoreBoardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('people', [ApiController::class, 'people'])->name('people');
Route::get('films', [ApiController::class, 'films'])->name('films');
Route::get('starships', [ApiController::class, 'starships'])->name('starships');
Route::get('vehicles', [ApiController::class, 'vehicles'])->name('vehicles');
Route::get('species', [ApiController::class, 'species'])->name('species');
Route::get('planets', [ApiController::class, 'planets'])->name('planets');

Route::get('people/{id}', [ApiController::class, 'peopleById'])->name('peopleById');
Route::get('films/{id}', [ApiController::class, 'filmsById'])->name('filmsById');
Route::get('starships/{id}', [ApiController::class, 'starshipsById'])->name('starshipsById');
Route::get('vehicles/{id}', [ApiController::class, 'vehiclesById'])->name('vehiclesById');
Route::get('species/{id}', [ApiController::class, 'speciesById'])->name('speciesById');
Route::get('planets/{id}', [ApiController::class, 'planetsById'])->name('planetsById');

Route::prefix('random')->group(function () {
    Route::get('character', [SwCardGameController::class, 'getRandomCharacter'])->name('randomCharacter');
    Route::get('starship', [SwCardGameController::class, 'getRandomStarship'])->name('randomStarship');
});

Route::prefix('board')->group(function () {
    Route::post('user', [SwScoreBoardController::class, 'createUser'])->name('createUser');
    Route::post('update', [SwScoreBoardController::class, 'updateScore'])->name('updateScore');
    Route::get('board', [SwScoreBoardController::class, 'getScoreBoard'])->name('getScoreBoard');
    Route::delete('remove', [SwScoreBoardController::class, 'removeUser'])->name('removeUser');
});
