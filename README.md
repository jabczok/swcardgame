## Installation guide for Debian 10
### Requirements
- Docker
- Git

### Instructions

* Download repository from git:

```bash
git clone git@bitbucket.org:jabczok/swcardgame.git
```

* Move to app dir:

```bash
cd swcardgame
```

* Use Docker’s composer image to download project dependencies:

```bash
sudo docker run --rm -v $(pwd):/app composer install
```

* Set permissions on the project directory so that it is owned by your non-root user:

```bash
sudo chown -R $USER:$USER $PATH_TO_API/swcardgame
```

* Copy .env.example into .env:

```bash
cp .env.example .env
```

* Edit .env file by adding Database credentials (Docker will use them to initialize database).

* Run docker-compose to build images (-d  flag daemonizes the process, running your containers in the background):

```bash
sudo docker-compose up -d
```

* Run following commands to generate app tokens and initialize database tables:

```bash
sudo docker-compose exec app php artisan key:generate
sudo docker-compose exec app php artisan config:cache
sudo docker-compose exec app php artisan migrate
```

* Initialize API database by running following comman:

```bash
sudo docker-compose exec app php artisan api:init
```

Now you should be able to see laravel welcome page on your local address:

![image info](./laravel_welcome.png)

## Api documentation
swagger documentation available on localhost/api/documentation
