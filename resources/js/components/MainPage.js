import React, {useEffect} from 'react'
import {Link} from 'react-router-dom'

const MainPage = () => {

    const textRef = React.createRef()

    useEffect(() => {
        animateText();
    }, []);

    const animateText = () => {
        let bylineText = textRef.current.innerHTML;
        let bylineArr = bylineText.split('');
        textRef.current.innerHTML = '';
        var span;
        var letter;

        for (let i = 0; i < bylineArr.length; i++) {
            span = document.createElement("span");
            letter = document.createTextNode(bylineArr[i]);
            if (bylineArr[i] == ' ') {
                byline.appendChild(letter);
            } else {
                span.appendChild(letter);
                byline.appendChild(span);
            }
        }
    }

    return (
        <div className='container'>
            <div className='row'>
                <div className='col-md-4 text-center'>
                    <Link to='/characters-battle'>
                        <button className='btn btn-primary'>Characters Battle</button>
                    </Link>
                </div>
                <div className='col-md-4 text-center'>
                    <Link to='/scores'>
                        <button className='btn btn-primary'>Score Table</button>
                    </Link>
                </div>
                <div className='col-md-4 text-center'>
                    <Link to='/starships-battle'>
                        <button className='btn btn-primary'>Starships Battle</button>
                    </Link>
                </div>
            </div>

            <div className="starwars-demo">
                <img src="//cssanimation.rocks/demo/starwars/images/star.svg" alt="Star" className="star"/>
                <img src="//cssanimation.rocks/demo/starwars/images/wars.svg" alt="Wars" className="wars"/>
                <h2 ref={textRef} className="byline" id="byline">The Card Game</h2>
            </div>
        </div>
    )
}

export default MainPage
