import React, {useState, useEffect} from 'react'

const ScoreTable = () => {

    const [state, setState] = useState({
        data: {
            data: [
                {id: 1, name: '', score: 0}
            ]
        }
    })

    useEffect(() => {
        getData();
    }, []);

    const handleClickNext = () => {
        getPage(state.data.next_page_url);
    }
    const handleClickPrev = () => {
        getPage(state.data.prev_page_url);
    }

    const getData = () => {
        axios.get('api/board/board', {}).then(response => {
            setState(() => {
                return {
                    data: response.data
                }
            })
        });
    }

    const getPage = (url) => {
        axios.get(url, {}).then(response => {
            setState(curState => {
                return {
                    data: response.data
                }
            })
        });
    }

    const removeUser = (name) => {
        axios.delete('api/board/remove', {
            data: {
                name: name
            }
        },).then(response => {
            if (response.data == 1) {
                getData()
            }
        });
    }

    const drawHead = () => {
        return (
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Score</th>
                <th scope="col">Delete</th>
            </tr>
            </thead>
        )
    }

    const drawBody = () => {
        return state.data.data.map((player, index) => {
            const {id, name, score} = player //destructuring
            return (
                <tr key={id}>
                    <td>{id}</td>
                    <td>{name}</td>
                    <td>{score}</td>
                    <td>
                        <button onClick={() => removeUser(name)} className='btn btn-danger'>Remove</button>
                    </td>
                </tr>
            )
        })
    }

    const drawTable = () => {

        return (
            <table className="table table-dark">
                {drawHead()}
                <tbody>
                {drawBody()}
                </tbody>
            </table>
        )
    }

    const drawNextPageButton = () => {
        if (state.data.next_page_url) {
            return (
                <button className='btn btn-primary' onClick={handleClickNext.bind(this)}>Next</button>
            )
        }
    }

    const drawPreviousPageButton = () => {
        if (state.data.prev_page_url) {
            return (
                <button className='btn btn-primary' onClick={handleClickPrev.bind(this)}>Previous</button>
            )
        }
    }

    return (
        <div className='container'>
            <div className='row'>
                <div className='col-md-12'>
                    {drawTable()}
                </div>
            </div>
            <div className='row'>
                <div className='col-md-12'>
                    <div className='float-right'>
                        {drawPreviousPageButton()}
                        {drawNextPageButton()}
                    </div>
                </div>
            </div>
        </div>
    )

}

export default ScoreTable
