import React from 'react'
import Battle from "./Battle";
import RegisterForm from "../RegisterForm";
import CharactersBattlefield from "./Battlefields/CharactersBattlefield";

class CharactersBattle extends Battle {

    constructor() {
        super();
        this.state.endpoint = 'api/random/character'
        this.state.left = {name: ''}
        this.state.right = {name: ''}
    }

    findWinner(state) {
        let leftStats = {
            mass: parseFloat(state.left.mass.replace(/,/g, '')),
            height: parseFloat(state.left.height.replace(/,/g, ''))
        };
        let rightStats = {
            mass: parseFloat(state.right.mass.replace(/,/g, '')),
            height: parseFloat(state.right.height.replace(/,/g, ''))
        };
        if (!Number.isNaN(leftStats.mass) && !Number.isNaN(rightStats.mass)) {
            return leftStats.mass > rightStats.mass ? state.left : state.right;
        }
        if (!Number.isNaN(leftStats.height) && !Number.isNaN(rightStats.height)) {
            return leftStats.height > rightStats.height ? state.left : state.right;
        }
        return Math.floor(Math.random()) === 1 ? state.left : state.right;
    }

    getContent(){
        const battlefieldData = {
            status: this.state.status,
            currWinner: this.state.currWinner,
            left: this.state.left,
            right: this.state.right,
            handleClickFight: this.handleClickFight,
            handleClickNewGame: this.handleClickNewGame
        }

        const playerStatus = this.state.status;
        let content;
        if (playerStatus == 'login') {
            content = <RegisterForm handleSubmit={this.handleSubmit}/>;
        } else {
            content = <CharactersBattlefield data={battlefieldData}/> ;
        }
        return content;
    }
}

export default CharactersBattle
