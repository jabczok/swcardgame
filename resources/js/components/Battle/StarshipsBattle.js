import React from 'react'
import Battle from "./Battle";
import RegisterForm from "../RegisterForm";
import StarshipsBattlefield from "./Battlefields/StarshipsBattlefield";

class StarshipsBattle extends Battle {

    constructor() {
        super();
        this.state.endpoint = 'api/random/starship'
        this.state.left = {model: ''}
        this.state.right = {model: ''}
    }

    findWinner(state) {
        let leftStats = {
            crew: parseFloat(state.left.crew.replace(/,/g, '')),
            cost_in_credits: parseFloat(state.left.cost_in_credits.replace(/,/g, ''))
        };
        let rightStats = {
            crew: parseFloat(state.right.crew.replace(/,/g, '')),
            cost_in_credits: parseFloat(state.right.cost_in_credits.replace(/,/g, ''))
        };
        if (!Number.isNaN(leftStats.crew) && !Number.isNaN(rightStats.crew)) {
            return leftStats.crew > rightStats.crew ? state.left : state.right;
        }
        if (!Number.isNaN(leftStats.cost_in_credits) && !Number.isNaN(rightStats.cost_in_credits)) {
            return leftStats.cost_in_credits > rightStats.cost_in_credits ? state.left : state.right;
        }
        return Math.floor(Math.random()) === 1 ? state.left : state.right;
    }

    getContent(){
        const battlefieldData = {
            status: this.state.status,
            currWinner: this.state.currWinner,
            left: this.state.left,
            right: this.state.right,
            handleClickFight: this.handleClickFight,
            handleClickNewGame: this.handleClickNewGame
        }

        const playerStatus = this.state.status;
        let content;
        if (playerStatus == 'login') {
            content = <RegisterForm handleSubmit={this.handleSubmit}/>;
        } else {
            content = <StarshipsBattlefield data={battlefieldData}/> ;
        }
        return content;
    }
}

export default StarshipsBattle
