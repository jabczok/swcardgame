import React, {Component} from 'react'
import ScoreBoard from "./Battlefields/ScoreBoard";

class Battle extends Component {

    constructor() {
        super();
        this.state = {
            endpoint: '',
            status: 'login',
            currWinner: null,
            leftScore: 0,
            rightScore: 0,
            left: {},
            right: {},
            player1: {name: ''},
            player2: {name: ''}
        }
        this.handleClickFight = this.handleClickFight.bind(this)
        this.handleClickNewGame = this.handleClickNewGame.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleSubmit(event) {
        event.preventDefault();
        const data = new FormData(event.target);
        let player1 = data.get('player1');
        let player2 = data.get('player2');

        axios.post('api/board/user', {
            name: player1
        }).then(response => {
            this.setState(curState => {
                return {
                    player1: response.data,
                    leftScore: response.data.score ? response.data.score : 0
                }
            })
        });

        axios.post('api/board/user', {
            name: player2
        }).then(response => {
            this.setState(curState => {
                return {
                    player2: response.data,
                    rightScore: response.data.score ? response.data.score : 0,
                    status: 'prepare'
                }
            })
        });
        this.getCards();
    }

    checkPlayers() {
        return this.state.player1.name == '' || this.state.player2.name == '';
    }

    updateScore(player, value) {
        axios.post('api/board/update', {
            name: player,
            value: value
        });
    }

    getCards() {
        axios.get(this.state.endpoint).then(response => {
            this.setState(curState => {
                return {
                    status: 'prepare',
                    currWinner: null,
                    left: response.data,
                }
            })
        });
        axios.get(this.state.endpoint).then(response => {
            this.setState(curState => {
                return {
                    status: 'prepare',
                    currWinner: null,
                    right: response.data
                }
            })
        });
    }

    findWinner(state) {
    }

    fight() {
        const newWinner = this.findWinner(this.state)
        if (newWinner === this.state.left) {
            this.updateScore(this.state.player1.name, 1);
        } else {
            this.updateScore(this.state.player2.name, 1);
        }
        this.setState(curState => {
            return {
                status: "fight",
                currWinner: newWinner,
                leftScore: (newWinner === curState.left ? curState.leftScore + 1 : curState.leftScore),
                rightScore: (newWinner === curState.right ? curState.rightScore + 1 : curState.rightScore),
            }
        })
    }

    newGame() {
        this.getCards()
    }

    handleClickNewGame() {
        this.newGame();
    }

    handleClickFight() {
        this.fight()
    }

    getContent() {

    }

    render() {
        return (
            <div className='container'>
                {this.getContent()}
                <ScoreBoard leftScore={this.state.leftScore} rightScore={this.state.rightScore}
                            player1={this.state.player1} player2={this.state.player2}/>
            </div>
        )
    }
}

export default Battle
