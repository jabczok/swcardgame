import React from 'react'

const StarshipsBattlefield = ({data: {status, currWinner, left, right, handleClickFight, handleClickNewGame}}) => (

    <div className='row'>
        <div className='col-md-5 text-center'>
            <img
                className={`avatar ${currWinner === right ? "grey-out" : ""} ${currWinner === left ? "winner" : ""}`}
                src={left.img_url} onError={(e) => {
                e.target.onerror = null;
                e.target.src = "/img/big-placeholder.jpg"
            }}/>
            <img className={`light-sabers starship-sabers ${currWinner === right ? "show-sabers" : ""}`}
                 src='img/lightsabers.png'></img>
            <p className={`winner-text starship-winner ${currWinner === left ? "show-winner" : ""}`}>WINNER</p>
            <p className='card-desc'>{left.model}</p>
        </div>
        <div className='col-md-2 text-center'>
            {status === "prepare" &&
            <button onClick={handleClickFight} className='btn btn-danger'>Fight!</button>}
            {status === "fight" &&
            <button onClick={handleClickNewGame} className='btn btn-primary'>New fight!</button>}
        </div>
        <div className='col-md-5 text-center'>
            <img
                className={`avatar ${currWinner === left ? "grey-out" : ""} ${currWinner === right ? "winner" : ""}`}
                src={right.img_url} onError={(e) => {
                e.target.onerror = null;
                e.target.src = "/img/big-placeholder.jpg"
            }}/>
            <img className={`light-sabers starship-sabers ${currWinner === left ? "show-sabers" : ""}`}
                 src='img/lightsabers.png'></img>
            <p className={`winner-text starship-winner ${currWinner === right ? "show-winner" : ""}`}>WINNER</p>
            <p className='card-desc'>{right.model}</p>
        </div>
    </div>
)

export default StarshipsBattlefield
