import React from "react";

const ScoreBoard = ({player1, player2, leftScore, rightScore}) => (
    <div className='row'>
        <div className='col-md-12'>
            <p>{player1.name} score: {leftScore}</p>
            <p>{player2.name} score: {rightScore}</p>
        </div>
    </div>
);

export default ScoreBoard;
