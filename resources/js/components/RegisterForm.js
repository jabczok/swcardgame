import React from 'react'

const RegisterForm = ({handleSubmit}) => (
    <div className='row'>
        <div className='col-md-12'>
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <label htmlFor="player1">Player 1</label>
                    <input type="text" className="form-control" id="player1" name="player1"
                           aria-describedby="playerHelp" placeholder="Enter player1 name"/>
                </div>
                <div className="form-group">
                    <label htmlFor="player2">Player 2</label>
                    <input type="text" className="form-control" id="player2" name="player2"
                           aria-describedby="playerHelp" placeholder="Enter player2 name"/>
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
)

export default RegisterForm
