import React, { Component } from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Header from './Header'
import MainPage from "./MainPage";
import CharactersBattle from "./Battle/CharactersBattle";
import StarshipsBattle from "./Battle/StarshipsBattle";
import ScoreTable from "./ScoreTable";

class App extends Component {
    render () {
        return (
            <BrowserRouter>
                <div>
                    <Header />
                    <Switch>
                        <Route exact path='/' component={MainPage} />
                        <Route exact path='/characters-battle' component={CharactersBattle} />
                        <Route exact path='/starships-battle' component={StarshipsBattle} />
                        <Route exact path='/scores' component={ScoreTable} />
                    </Switch>
                </div>
            </BrowserRouter>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('app'))
