<?php

namespace App\Console\Commands;

use App\Models\Film;
use App\Models\Person;
use App\Models\Planet;
use App\Models\Species;
use App\Models\Starship;
use App\Models\Vehicle;
use App\Services\ApiInitService;
use Illuminate\Console\Command;

class initApiDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'api:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initialize api database, data will be crawled from https://swapi.dev/';

    /**
     * Create a new command instance.
     *
     * @return void
     */

    private $initService;

    public function __construct()
    {
        parent::__construct();
        $this->initService = new ApiInitService();
    }

    public function handle()
    {
        if (!$this->checkDbData()) {
            return;
        }
        try {
            $people = $this->initService->getPeople();
            $films = $this->initService->getFilms();
            $planets = $this->initService->getPlanets();
            $species = $this->initService->getSpecies();
            $starships = $this->initService->getStarships();
            $vehicles = $this->initService->getVehicles();
        } catch (\Exception $e) {
            throw new \Exception('Error while collecting init data!' . PHP_EOL . $e->getMessage());
        }

        foreach ($planets as $planet) {
            $planet->save();
        }
        $peopleRelations = [];
        foreach ($people as $person) {
            $pFilms = $person->films;
            $pStarships = $person->starships;
            $pVehicles = $person->vehicles;
            $pSpecies = $person->species;
            unset($person->films);
            unset($person->starships);
            unset($person->vehicles);
            unset($person->species);
            if ($person->homeworld > 0) {
                $person->homeworld = $planets[$person->homeworld]->id;
            } else {
                $person->homeworld  = null;
            }
            $person->save();
            $peopleRelations['films'][$person->id] = $pFilms;
            $peopleRelations['starships'][$person->id] = $pStarships;
            $peopleRelations['vehicles'][$person->id] = $pVehicles;
            $peopleRelations['species'][$person->id] = $pSpecies;
        }
        $filmsRelations = [];
        foreach ($films as $film) {
            $fVehicles = $film->vehicles;
            $fStarships = $film->starships;
            $fSpecies = $film->species;
            $fPlanets = $film->planets;
            unset($film->vehicles);
            unset($film->starships);
            unset($film->species);
            unset($film->planets);
            $film->save();
            $filmsRelations['starships'][$film->id] = $fStarships;
            $filmsRelations['vehicles'][$film->id] = $fVehicles;
            $filmsRelations['species'][$film->id] = $fSpecies;
            $filmsRelations['planets'][$film->id] = $fPlanets;
        }
        foreach ($species as $type) {
            if ($type->homeworld > 0) {
                $type->homeworld = $planets[$type->homeworld]->id;
            } else {
                $type->homeworld  = null;
            }
            $type->save();
        }
        foreach ($starships as $starship) {
            $starship->save();
        }
        foreach ($vehicles as $vehicle) {
            $vehicle->save();
        }

        foreach ($peopleRelations['films'] as $key => $relations) {
            $person = Person::whereId($key)->first();
            if ($person) {
                foreach ($relations as $externalId) {
                    $person->films()->attach($films[$externalId]->id);
                }
            }
        }
        foreach ($peopleRelations['starships'] as $key => $relations) {
            $person = Person::whereId($key)->first();
            if ($person) {
                foreach ($relations as $externalId) {
                    $person->starships()->attach($starships[$externalId]->id);
                }
            }
        }
        foreach ($peopleRelations['vehicles'] as $key => $relations) {
            $person = Person::whereId($key)->first();
            if ($person) {
                foreach ($relations as $externalId) {
                    $person->vehicles()->attach($vehicles[$externalId]->id);
                }
            }
        }
        foreach ($peopleRelations['species'] as $key => $relations) {
            $person = Person::whereId($key)->first();
            if ($person) {
                foreach ($relations as $externalId) {
                    $person->species()->attach($species[$externalId]->id);
                }
            }
        }
        foreach ($filmsRelations['starships'] as $key => $relations) {
            $film = Film::whereId($key)->first();
            if ($film) {
                foreach ($relations as $externalId) {
                    $film->starships()->attach($starships[$externalId]->id);
                }
            }
        }
        foreach ($filmsRelations['vehicles'] as $key => $relations) {
            $film = Film::whereId($key)->first();
            if ($film) {
                foreach ($relations as $externalId) {
                    $film->vehicles()->attach($vehicles[$externalId]->id);
                }
            }
        }
        foreach ($filmsRelations['species'] as $key => $relations) {
            $film = Film::whereId($key)->first();
            if ($film) {
                foreach ($relations as $externalId) {
                    $film->species()->attach($species[$externalId]->id);
                }
            }
        }
        foreach ($filmsRelations['planets'] as $key => $relations) {
            $film = Film::whereId($key)->first();
            if ($film) {
                foreach ($relations as $externalId) {
                    $film->planets()->attach($planets[$externalId]->id);
                }
            }
        }
    }

    private function checkDbData()
    {
        $people = Person::all();
        $films = Film::all();
        $planets = Planet::all();
        $species = Species::all();
        $starships = Starship::all();
        $vehicles = Vehicle::all();
        if ($people->count() > 0
            || $films->count() > 0
            || $planets->count() > 0
            || $species->count() > 0
            || $starships->count() > 0
            || $vehicles->count() > 0
        ) {
            $clear = $this->ask('Data in database detected, do you want to remove it and initialize again? Y/N', "N");
            if ($clear === 'Y' || $clear === "y") {
                foreach ($people as $person) {
                    $person->delete();
                }
                foreach ($films as $film) {
                    $film->delete();
                }
                foreach ($planets as $planet) {
                    $planet->delete();
                }
                foreach ($species as $type) {
                    $type->delete();
                }
                foreach ($starships as $starship) {
                    $starship->delete();
                }
                foreach ($vehicles as $vehicle) {
                    $vehicle->delete();
                }
                return true;
            }
            return false;
        } else {
            return true;
        }
    }
}
