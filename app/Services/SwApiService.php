<?php

namespace App\Services;

use App\Services\Request\FilmsRequest;
use App\Services\Request\PeopleRequest;
use App\Services\Request\PlanetsRequest;
use App\Services\Request\SpeciesRequest;
use App\Services\Request\StarshipsRequest;
use App\Services\Request\VehiclesRequest;
use GuzzleHttp\Client;

class SwApiService
{
    private $httpClient;
    const SWAPI_URL = "https://swapi.dev/api/";

    public function __construct()
    {
        $this->httpClient = new Client([
            'base_uri' => self::SWAPI_URL,
            'default' => [
                'exceptions' => false,
                'headers' => [
                    'Accept' => 'application/json',
                ],
            ],
        ]);
    }

    public function films(): FilmsRequest
    {
        return new FilmsRequest($this->httpClient);
    }

    public function people(): PeopleRequest
    {
        return new PeopleRequest($this->httpClient);
    }

    public function planets(): PlanetsRequest
    {
        return new PlanetsRequest($this->httpClient);
    }

    public function species(): SpeciesRequest
    {
        return new SpeciesRequest($this->httpClient);
    }

    public function starships(): StarshipsRequest
    {
        return new StarshipsRequest($this->httpClient);
    }

    public function vehicles(): VehiclesRequest
    {
        return new VehiclesRequest($this->httpClient);
    }
}
