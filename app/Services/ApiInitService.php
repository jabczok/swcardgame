<?php


namespace App\Services;


use App\Helpers\UrlHelpers;
use App\Models\Film;
use App\Models\Person;
use App\Models\Planet;
use App\Models\Species;
use App\Models\Starship;
use App\Models\Vehicle;

class ApiInitService
{
    protected $swapi;

    public function __construct()
    {
        $this->swapi = new SwApiService();
    }

    public function getPeople()
    {
        $ids = $this->swapi->people()->getAllIds();
        $people = [];
        foreach ($ids as $id) {
            $data = $this->swapi->people()->byId($id);
            UrlHelpers::massUrlToIdChange($data['vehicles']);
            UrlHelpers::massUrlToIdChange($data['starships']);
            UrlHelpers::massUrlToIdChange($data['films']);
            UrlHelpers::massUrlToIdChange($data['species']);
            $data['homeworld'] = (int) UrlHelpers::getIdFromUrl($data['homeworld']);
            $data['img_url'] = UrlHelpers::getImgUrlForCharacter($id);
            $person = new Person($data);
            $people[$id] = $person;
        }
        return $people;
    }

    public function getFilms()
    {
        $ids = $this->swapi->films()->getAllIds();
        $films = [];
        foreach ($ids as $id) {
            $data = $this->swapi->films()->byId($id);
            UrlHelpers::massUrlToIdChange($data['vehicles']);
            UrlHelpers::massUrlToIdChange($data['starships']);
            UrlHelpers::massUrlToIdChange($data['species']);
            UrlHelpers::massUrlToIdChange($data['planets']);
            UrlHelpers::massUrlToIdChange($data['characters']);
            $film = new Film($data);
            $films[$id] = $film;
        }
        return $films;
    }

    public function getStarships()
    {
        $ids = $this->swapi->starships()->getAllIds();
        $starships = [];
        foreach ($ids as $id) {
            $data = $this->swapi->starships()->byId($id);
            $data['img_url'] = UrlHelpers::getImgUrlForStarship($id);
            $starship = new Starship($data);
            $starships[$id] = $starship;
        }
        return $starships;
    }

    public function getVehicles()
    {
        $ids = $this->swapi->vehicles()->getAllIds();
        $vehicles = [];
        foreach ($ids as $id) {
            $data = $this->swapi->vehicles()->byId($id);
            $vehicle = new Vehicle($data);
            $vehicles[$id] = $vehicle;
        }
        return $vehicles;
    }

    public function getSpecies()
    {
        $ids = $this->swapi->species()->getAllIds();
        $species = [];
        foreach ($ids as $id) {
            $data = $this->swapi->species()->byId($id);
            $data['homeworld'] = (int) UrlHelpers::getIdFromUrl($data['homeworld']);
            $type = new Species($data);
            $species[$id] = $type;
        }
        return $species;
    }

    public function getPlanets()
    {
        $ids = $this->swapi->planets()->getAllIds();
        $planets = [];
        foreach ($ids as $id) {
            $data = $this->swapi->planets()->byId($id);
            $planet = new Planet($data);
            $planets[$id] = $planet;
        }
        return $planets;
    }
}
