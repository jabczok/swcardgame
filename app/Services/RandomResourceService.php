<?php

namespace App\Services;

use App\Models\Person;
use App\Models\Starship;

class RandomResourceService
{

    public function getRandomCharacterData()
    {
        return Person::inRandomOrder()->first();
    }

    public function getRandomStarshipData()
    {
        return Starship::inRandomOrder()->first();
    }
}
