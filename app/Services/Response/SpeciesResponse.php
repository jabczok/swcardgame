<?php


namespace App\Services\Response;


use App\Models\Species;

class SpeciesResponse extends Response
{
    protected $model = Species::class;
    protected $relations = [
        'people',
        'homeworld'
    ];
}
