<?php


namespace App\Services\Response;


use App\Models\Person;

class PeopleResponse extends Response
{
    protected $model = Person::class;
    protected $relations = [
        'films',
        'species',
        'starships',
        'vehicles',
        'homeworld',
    ];
}
