<?php


namespace App\Services\Response;


use App\Models\Planet;

class PlanetsRespone extends Response
{
    protected $model = Planet::class;
    protected $relations = [
        'residents',
        'films'
    ];
}
