<?php


namespace App\Services\Response;


use App\Models\Starship;

class StarshipsResponse extends Response
{
    protected $model = Starship::class;
    protected $relations = [
        'pilots',
        'films'
    ];
}
