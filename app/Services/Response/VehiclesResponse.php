<?php


namespace App\Services\Response;

use App\Models\Vehicle;

class VehiclesResponse extends Response
{
    protected $model = Vehicle::class;
    protected $relations = [
        'pilots',
        'films'
    ];
}
