<?php


namespace App\Services\Response;


class Response
{
    protected $model;
    protected $relations;

    private $instance;
    const PER_PAGE = 10;

    public function __construct()
    {
        $this->instance = new $this->model;
    }

    public function getAllData()
    {
        return $this->instance->with($this->relations)->paginate(self::PER_PAGE);
    }

    public function getById($id){
        return $this->instance->whereId($id)->with($this->relations)->first();
    }
}
