<?php


namespace App\Services\Response;


use App\Models\Film;

class FilmsResponse extends Response
{
    protected $model = Film::class;
    protected $relations = [
        'characters',
        'species',
        'starships',
        'vehicles',
        'planets',
    ];
}
