<?php


namespace App\Services;


use App\Services\Response\FilmsResponse;
use App\Services\Response\PeopleResponse;
use App\Services\Response\PlanetsRespone;
use App\Services\Response\SpeciesResponse;
use App\Services\Response\StarshipsResponse;
use App\Services\Response\VehiclesResponse;

class ApiService
{
    public function people()
    {
        return new PeopleResponse();
    }

    public function films()
    {
        return new FilmsResponse();
    }

    public function starships()
    {
        return new StarshipsResponse();
    }

    public function vehicles()
    {
        return new VehiclesResponse();
    }

    public function species()
    {
        return new SpeciesResponse();
    }

    public function planets()
    {
        return new PlanetsRespone();
    }
}
