<?php


namespace App\Services;


use App\Models\Player;
use Illuminate\Http\Request;

class ScoreBoardService
{
    public function createUser(Request $request)
    {
        $player = Player::whereName($request->name)->first();
        if ($player) {
            return $player;
        } else {
            $player = new Player($request->all());
            $player->save();
            return $player;
        }
    }

    public function updateScore(Request $request)
    {
        $player = Player::whereName($request->name)->first();
        if ($player) {
            $player->score = $player->score + $request->value;
            $player->save();
            return $player;
        } else {
            return null;
        }
    }

    public function getScoreBoard()
    {
        return Player::select('id','name','score')->orderBy('score', 'desc')->paginate();
    }

    public function removeUser(Request $request)
    {
        $player = Player::whereName($request->name)->first();
        if ($player) {
            $player->delete();
            return true;
        } else {
            return false;
        }
    }
}
