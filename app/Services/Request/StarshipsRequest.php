<?php


namespace App\Services\Request;

use App\Helpers\UrlHelpers;
use Illuminate\Support\Facades\Cache;

class StarshipsRequest extends Request
{
    /**
     * @var string
     */
    protected $path = 'starships';
}
