<?php


namespace App\Services\Request;


use App\Helpers\UrlHelpers;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cache;
use Psr\Http\Message\ResponseInterface;

abstract class Request
{
    /**
     * @var Client
     */
    protected $httpClient;

    /**
     * @var string
     */
    protected $path = '';

    /**
     * @var string
     */
    protected $resource = '';

    /**
     * @param Client $httpClient
     */
    public function __construct(Client $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param int $page
     * @return array|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function all(int $page = 1): ?array
    {
        if (Cache::has($this->path . '-page-' . $page)) {
            return Cache::get($this->path . '-page-' . $page);;
        } else {
            $response = $this->httpClient->request(
                'GET',
                sprintf('%s/?page=%s', $this->path, $page),
                ['http_errors' => false]
            );
            $processedResponse = $this->processResponse($response);
            Cache::put($this->path . '-page-' . $page, $processedResponse, 86400);
            return $processedResponse;
        }
    }

    /**
     * @param int $id
     * @return array|null
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function byId(int $id): ?array
    {
        if (Cache::has($this->path . '-id-' . $id)) {
            return Cache::get($this->path . '-id-' . $id);
        } else {
            $response = $this->httpClient->request(
                'GET',
                sprintf('%s/%s/', $this->path, $id),
                ['http_errors' => false]
            );
            $processedResponse = $this->processResponse($response);
            Cache::put($this->path . '-id-' . $id, $processedResponse, 86400);
            return $processedResponse;
        }
    }

    public function getRandomId()
    {
        $ids = $this->getAllIds();
        return $ids[array_rand($ids)];
    }

    public function getAllIds(){
        $ids = [];
        if (Cache::has($this->path . '-ids')) {
            $ids = Cache::get($this->path . '-ids');
        } else {
            $nextId = 1;
            while ($nextId != null) {
                $response = $this->all($nextId);
                foreach ($response['results'] as $res) {
                    $id = UrlHelpers::getIdFromUrl($res['url']);
                    $ids[] = $id;
                }
                $nextId = UrlHelpers::getIdFromPageUrl($response['next']);
            }
            Cache::put($this->path . '-ids', $ids, 86400);
        }
        return $ids;
    }

    /**
     * @param ResponseInterface $response
     * @return array|null
     */
    protected function processResponse(ResponseInterface $response): ?array
    {
        switch ($response->getStatusCode()) {
            case 200:
                return json_decode($response->getBody()->getContents(), true);
            default:
                return null;
        }
    }
}
