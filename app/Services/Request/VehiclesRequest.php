<?php


namespace App\Services\Request;

class VehiclesRequest extends Request
{
    /**
     * @var string
     */
    protected $path = 'vehicles';
}
