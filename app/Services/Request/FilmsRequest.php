<?php


namespace App\Services\Request;

class FilmsRequest extends Request
{
    /**
     * @var string
     */
    protected $path = 'films';
}
