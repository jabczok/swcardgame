<?php


namespace App\Services\Request;

class PlanetsRequest extends Request
{
    /**
     * @var string
     */
    protected $path = 'planets';
}
