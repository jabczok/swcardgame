<?php


namespace App\Services\Request;

class SpeciesRequest extends Request
{
    /**
     * @var string
     */
    protected $path = 'species';
}
