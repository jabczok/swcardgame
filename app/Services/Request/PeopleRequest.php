<?php


namespace App\Services\Request;

class PeopleRequest extends Request
{
    /**
     * @var string
     */
    protected $path = 'people';
}
