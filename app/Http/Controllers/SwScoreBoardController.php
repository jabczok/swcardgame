<?php


namespace App\Http\Controllers;

use App\Services\ScoreBoardService;
use Illuminate\Http\Request;

class SwScoreBoardController extends Controller
{
    private $sbService;

    public function __construct()
    {
        $this->sbService = new ScoreBoardService();
    }

    /**
     ** @OA\Post(path="/api/board/user",
     *     tags={"board"},
     *     @OA\RequestBody(
     *         description="User name that need to be stored or recieved",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         ),
     *     ),
     *   @OA\Response(
     *     response=200,
     *     description="Returns user data and create if not existing",
     *     content={
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          example=
     *           {
     *              "id": 1,
     *              "name": "test",
     *              "score": 0,
     *              "created_at": "2020-11-19T20:48:43.000000Z",
     *              "updated_at": "2020-11-19T20:48:43.000000Z"
    *           }
     *      )
     *     }
     *     )
     *   )
     * @OA\Link(link="api/board/user")
     */
    public function createUser(Request $request)
    {
        return $this->sbService->createUser($request);
    }

    /**
     ** @OA\Post(path="/api/board/update",
     *     tags={"board"},
     *     @OA\RequestBody(
     *         description="User name and value to increase score.",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         ),
     *     ),
     *   @OA\Response(
     *     response=200,
     *     description="Increse user score of value number.",
     *     content={
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          example=
     *           {
     *              "id": 1,
     *              "name": "test",
     *              "score": 0,
     *              "created_at": "2020-11-19T20:48:43.000000Z",
     *              "updated_at": "2020-11-19T20:48:43.000000Z"
     *           }
     *      )
     *     }
     *     )
     *   )
     * @OA\Link(link="api/board/update")
     */
    public function updateScore(Request $request)
    {
        $response = $this->sbService->updateScore($request);
        if ($response) {
            return $response;
        }
        return response()->json(['error' => 'User does not exist!'], 404);
    }


    /**
     ** @OA\Get(path="/api/board/board",
     *     tags={"board"},
     *     @OA\Parameter(
     *         description="ID of paginated page",
     *         in="path",
     *         name="id",
     *         required=false,
     *         @OA\Schema(
     *           type="integer"
     *         )
     *     ),
     *   @OA\Response(
     *     response=200,
     *     description="Returns paginated players data.",
     *     content={
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          example=
     *           {
    *           "current_page": 2,
    *           "data": {
    *           {
    *           "id": 17,
    *           "name": "2345",
    *           "score": 0
    *           },
    *           {
    *           "id": 18,
    *           "name": "532",
    *           "score": 0
    *           },
    *           {
    *           "id": 19,
    *           "name": "n6335",
    *           "score": 0
    *           },
    *           {
    *           "id": 20,
    *           "name": "h6n35n6",
    *           "score": 0
    *           },
    *           {
    *           "id": 21,
    *           "name": "h3r456t",
    *           "score": 0
    *           },
    *           {
    *           "id": 22,
    *           "name": "56432631",
    *           "score": 0
    *           },
    *           {
    *           "id": 23,
    *           "name": "2457",
    *           "score": 0
    *           },
    *           {
    *           "id": 24,
    *           "name": "257",
    *           "score": 0
    *           },
    *           {
    *           "id": 25,
    *           "name": "63",
    *           "score": 0
    *           },
    *           {
    *           "id": 26,
    *           "name": "dwa",
    *           "score": 0
    *           },
    *           {
    *           "id": 27,
    *           "name": "1233",
    *           "score": 0
    *           },
    *           {
    *           "id": 28,
    *           "name": "asdw2",
    *           "score": 0
    *           },
    *           {
    *           "id": 29,
    *           "name": "tewe3",
    *           "score": 0
    *           },
    *           {
    *           "id": 30,
    *           "name": "sad",
    *           "score": 0
    *           },
    *           {
    *           "id": 31,
    *           "name": "312",
    *           "score": 0
    *           }
    *           },
    *           "first_page_url": "http://localhost/api/board/board?page=1",
    *           "from": 16,
    *           "last_page": 3,
    *           "last_page_url": "http://localhost/api/board/board?page=3",
    *           "links": {
    *           {
    *           "url": "http://localhost/api/board/board?page=1",
    *           "label": " Previous",
    *           "active": false
    *           },
    *           {
    *           "url": "http://localhost/api/board/board?page=1",
    *           "label": 1,
    *           "active": false
    *           },
    *           {
    *           "url": "http://localhost/api/board/board?page=2",
    *           "label": 2,
    *           "active": true
    *           },
    *           {
    *           "url": "http://localhost/api/board/board?page=3",
    *           "label": 3,
    *           "active": false
    *           },
    *           {
    *           "url": "http://localhost/api/board/board?page=3",
    *           "label": "Next ",
    *           "active": false
    *           }
    *           },
    *           "next_page_url": "http://localhost/api/board/board?page=3",
    *           "path": "http://localhost/api/board/board",
    *           "per_page": 15,
    *           "prev_page_url": "http://localhost/api/board/board?page=1",
    *           "to": 30,
    *           "total": 37
    *           }
     *      )
     *     }
     *     )
     *   )
     * @OA\Link(link="api/board/board")
     */
    public function getScoreBoard()
    {
        return $this->sbService->getScoreBoard();
    }

    /**
     ** @OA\Delete (path="/api/board/remove",
     *     tags={"board"},
     *     @OA\RequestBody(
     *         description="User name to remove",
     *         required=true,
     *         @OA\MediaType(
     *             mediaType="application/json",
     *         ),
     *     ),
     *   @OA\Response(
     *     response=200,
     *     description="Removes player",
     *     content={
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          example=
     *           {
     *             1
     *           }
     *      )
     *     }
     *     )
     *   )
     * @OA\Link(link="api/board/remove")
     */
    public function removeUser(Request $request)
    {
        $response = $this->sbService->removeUser($request);
        if ($response) {
            return $response;
        }
        return response()->json(['error' => 'User does not exist!'], 404);
    }
}
