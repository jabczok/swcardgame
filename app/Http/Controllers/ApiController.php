<?php


namespace App\Http\Controllers;


use App\Services\ApiService;
use Illuminate\Http\Request;

class ApiController extends Controller
{
    /**
     * @var ApiService
     */
    private $apiService;

    public function __construct()
    {
        $this->apiService = new ApiService();
    }

    /**
     * @OA\Get(
     * path="/api/people",
     * summary="All characters data",
     * description="Paginated characters data",
     * tags={"api"},
     * @OA\Response(
     *    response=200,
     *    description="Paginated characters data",
     *     )
     * )
     */
    public function people()
    {
        return $this->apiService->people()->getAllData();
    }

    /**
     * @OA\Get(
     * path="/api/films",
     * summary="All films data",
     * description="Paginated films data",
     * tags={"api"},
     * @OA\Response(
     *    response=200,
     *    description="Paginated films data",
     *     )
     * )
     */
    public function films()
    {
        return $this->apiService->films()->getAllData();
    }

    /**
     * @OA\Get(
     * path="/api/starships",
     * summary="All starships data",
     * description="Paginated starships data",
     * tags={"api"},
     * @OA\Response(
     *    response=200,
     *    description="Paginated starships data",
     *     )
     * )
     */
    public function starships()
    {
        return $this->apiService->starships()->getAllData();
    }

    /**
     * @OA\Get(
     * path="/api/vehicles",
     * summary="All vehicles data",
     * description="Paginated vehicles data",
     * tags={"api"},
     * @OA\Response(
     *    response=200,
     *    description="Paginated vehicles data",
     *     )
     * )
     */
    public function vehicles()
    {
        return $this->apiService->vehicles()->getAllData();
    }

    /**
     * @OA\Get(
     * path="/api/species",
     * summary="All species data",
     * description="Paginated species data",
     * tags={"api"},
     * @OA\Response(
     *    response=200,
     *    description="Paginated species data",
     *     )
     * )
     */
    public function species()
    {
        return $this->apiService->species()->getAllData();
    }

    /**
     * @OA\Get(
     * path="/api/planets",
     * summary="All planets data",
     * description="Paginated planets data",
     * tags={"api"},
     * @OA\Response(
     *    response=200,
     *    description="Paginated planets data",
     *     )
     * )
     */
    public function planets()
    {
        return $this->apiService->planets()->getAllData();
    }

    /**
     * @OA\Get(
     * path="/api/people/{id}",
     * summary="Returns data for character",
     * description="Returns character data for specific id",
     * tags={"api"},
     * @OA\Response(
     *    response=200,
     *    description="Characters data for specific id",
     *     ),
     *   @OA\Parameter(
     *      name="id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="integer"
     *      )
     *   ),
     * )
     */
    public function peopleById(Request $request)
    {
        return $this->apiService->people()->getById($request->id);
    }

    /**
     * @OA\Get(
     * path="/api/films/{id}",
     * summary="Returns data for film",
     * description="Returns film data for specific id",
     * tags={"api"},
     * @OA\Response(
     *    response=200,
     *    description="Films data for specific id",
     *     ),
     *   @OA\Parameter(
     *      name="id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="integer"
     *      )
     *   ),
     * )
     */
    public function filmsById(Request $request)
    {
        return $this->apiService->films()->getById($request->id);
    }

    /**
     * @OA\Get(
     * path="/api/starships/{id}",
     * summary="Returns data for starship",
     * description="Returns starship data for specific id",
     * tags={"api"},
     * @OA\Response(
     *    response=200,
     *    description="Starships data for specific id",
     *     ),
     *   @OA\Parameter(
     *      name="id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="integer"
     *      )
     *   ),
     * )
     */
    public function starshipsById(Request $request)
    {
        return $this->apiService->starships()->getById($request->id);
    }

    /**
     * @OA\Get(
     * path="/api/vehicles/{id}",
     * summary="Returns data for vehicle",
     * description="Returns vehicle data for specific id",
     * tags={"api"},
     * @OA\Response(
     *    response=200,
     *    description="Vehicles data for specific id",
     *     ),
     *   @OA\Parameter(
     *      name="id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="integer"
     *      )
     *   ),
     * )
     */
    public function vehiclesById(Request $request)
    {
        return $this->apiService->vehicles()->getById($request->id);
    }

    /**
     * @OA\Get(
     * path="/api/species/{id}",
     * summary="Returns data for species",
     * description="Returns species data for specific id",
     * tags={"api"},
     * @OA\Response(
     *    response=200,
     *    description="Species data for specific id",
     *     ),
     *   @OA\Parameter(
     *      name="id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="integer"
     *      )
     *   ),
     * )
     */
    public function speciesById(Request $request)
    {
        return $this->apiService->species()->getById($request->id);
    }

    /**
     * @OA\Get(
     * path="/api/planets/{id}",
     * summary="Returns data for planet",
     * description="Returns planet data for specific id",
     * tags={"api"},
     * @OA\Response(
     *    response=200,
     *    description="Planets data for specific id",
     *     ),
     *   @OA\Parameter(
     *      name="id",
     *      in="query",
     *      required=true,
     *      @OA\Schema(
     *           type="integer"
     *      )
     *   ),
     * )
     */
    public function planetsById(Request $request)
    {
        return $this->apiService->planets()->getById($request->id);
    }

}
