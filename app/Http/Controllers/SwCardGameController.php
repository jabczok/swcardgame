<?php

namespace App\Http\Controllers;


use App\Services\RandomResourceService;

class SwCardGameController extends Controller
{
    private $rrs;

    public function __construct()
    {
        $this->rrs = new RandomResourceService();
    }

    /**
     ** @OA\Get(path="/api/random/character",
     *     tags={"random"},
     *   @OA\Response(
     *     response=200,
     *     description="Returns random character data",
     *     content={
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          example=
     *          {
     *              "name": "Jocasta Nu",
     *               "height": "167",
     *               "mass": "unknown",
     *               "hair_color": "white",
     *               "skin_color": "fair",
     *               "eye_color": "blue",
     *               "birth_year": "unknown",
     *               "gender": "female",
     *               "homeworld": "http://swapi.dev/api/planets/9/",
     *               "films": {
     *               "http://swapi.dev/api/films/5/"
     *               },
     *               "species": {
     *               "http://swapi.dev/api/species/1/"
     *               },
     *               "vehicles": {},
     *               "starships": {},
     *               "created": "2014-12-20T17:32:51.996000Z",
     *               "edited": "2014-12-20T21:17:50.476000Z",
     *               "url": "http://swapi.dev/api/people/74/",
     *               "imgUrl": "https://starwars-visualguide.com/assets/img/characters/74.jpg"
     *               }
     *      )
     *     }
     *     )
     *   )
     * @OA\Link(link="api/random/character")
     */
    public function getRandomCharacter()
    {
        return $this->rrs->getRandomCharacterData();
    }

    /**
     ** @OA\Get(path="/api/random/starship",
     *     tags={"random"},
     *   @OA\Response(
     *     response=200,
     *     description="Returns random starship data",
     *     content={
     *      @OA\MediaType(
     *          mediaType="application/json",
     *          example=
     *           {
     *              "name": "Trade Federation cruiser",
     *              "model": "Providence-class carrier/destroyer",
     *              "manufacturer": "Rendili StarDrive, Free Dac Volunteers Engineering corps.",
     *              "cost_in_credits": "125000000",
     *              "length": "1088",
     *              "max_atmosphering_speed": "1050",
     *              "crew": "600",
     *              "passengers": "48247",
     *              "cargo_capacity": "50000000",
     *              "consumables": "4 years",
     *              "hyperdrive_rating": "1.5",
     *              "MGLT": "unknown",
     *              "starship_class": "capital ship",
     *              "pilots": {
     *                  "http://swapi.dev/api/people/10/",
     *                  "http://swapi.dev/api/people/11/"
     *              },
     *                  "films": {
     *                  "http://swapi.dev/api/films/6/"
     *              },
     *              "created": "2014-12-20T19:40:21.902000Z",
     *              "edited": "2014-12-20T21:23:49.941000Z",
     *              "url": "http://swapi.dev/api/starships/59/",
     *              "imgUrl": "https://starwars-visualguide.com/assets/img/starships/59.jpg"
     *            }
     *      )
     *     }
     *     )
     *   )
     * @OA\Link(link="api/random/starship")
     */
    public function getRandomStarship()
    {
        return $this->rrs->getRandomStarshipData();
    }
}
