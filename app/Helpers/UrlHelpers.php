<?php


namespace App\Helpers;


class UrlHelpers
{
    public static function getImgUrlForCharacter($id)
    {
        return "https://starwars-visualguide.com/assets/img/characters/" . $id . ".jpg";
    }

    public static function getImgUrlForStarship($id)
    {
        return "https://starwars-visualguide.com/assets/img/starships/" . $id . ".jpg";
    }

    public static function getIdFromUrl($url)
    {
        preg_match_all('#/([^/]*)/#', $url, $matches);

        return last(last($matches));
    }

    public static function getIdFromPageUrl($url)
    {
        preg_match_all('#page=([^/]*)#', $url, $matches);

        return last(last($matches));
    }

    public static function massUrlToIdChange(&$arr){
        if (!$arr){
            return;
        }
        foreach ($arr as $key => $url){
            $arr[$key] = UrlHelpers::getIdFromUrl($url);
        }
    }
}
