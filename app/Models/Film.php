<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Film
 *
 * @property int $id
 * @property string $title
 * @property int $episode_id
 * @property string|null $opening_crawl
 * @property string|null $director
 * @property string|null $producer
 * @property string|null $release_date
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Person[] $characters
 * @property-read int|null $characters_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Planet[] $planets
 * @property-read int|null $planets_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Species[] $species
 * @property-read int|null $species_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Starship[] $starships
 * @property-read int|null $starships_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vehicle[] $vehicles
 * @property-read int|null $vehicles_count
 * @method static \Illuminate\Database\Eloquent\Builder|Film newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Film newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Film query()
 * @method static \Illuminate\Database\Eloquent\Builder|Film whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Film whereDirector($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Film whereEpisodeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Film whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Film whereOpeningCrawl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Film whereProducer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Film whereReleaseDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Film whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Film whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Film extends Model
{
    use HasFactory;

    protected $hidden = ['pivot'];

    protected $fillable = [
        'title',
        'episode_id',
        'opening_crawl',
        'director',
        'producer',
        'release_date',
        'vehicles',
        'starships',
        'species',
        'planets',
    ];

    public function characters()
    {
        return $this->belongsToMany(
            Person::class,
            'people_films',
            'film_id',
            'person_id')->select('people.id');
    }

    public function species()
    {
        return $this->belongsToMany(
            Species::class,
            'films_species',
            'film_id',
            'species_id')->select('species.id');
    }

    public function starships()
    {
        return $this->belongsToMany(
            Starship::class,
            'films_starships',
            'film_id',
            'starship_id')->select('starships.id');
    }

    public function vehicles()
    {
        return $this->belongsToMany(
            Vehicle::class,
            'films_vehicles',
            'film_id',
            'vehicle_id')->select('vehicles.id');
    }

    public function planets()
    {
        return $this->belongsToMany(
            Planet::class,
            'films_planets',
            'film_id',
            'planet_id')->select('planets.id');
    }
}
