<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Species
 *
 * @property int $id
 * @property string $name
 * @property string|null $classification
 * @property string|null $designation
 * @property string|null $average_height
 * @property string|null $average_lifespan
 * @property string|null $eye_colors
 * @property string|null $hair_colors
 * @property string|null $skin_colors
 * @property string|null $language
 * @property \App\Models\Planet|null $homeworld
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Film[] $films
 * @property-read int|null $films_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Person[] $people
 * @property-read int|null $people_count
 * @method static \Illuminate\Database\Eloquent\Builder|Species newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Species newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Species query()
 * @method static \Illuminate\Database\Eloquent\Builder|Species whereAverageHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Species whereAverageLifespan($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Species whereClassification($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Species whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Species whereDesignation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Species whereEyeColors($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Species whereHairColors($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Species whereHomeworld($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Species whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Species whereLanguage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Species whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Species whereSkinColors($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Species whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Species extends Model
{
    use HasFactory;

    protected $hidden = ['pivot'];

    protected $fillable = [
        'name',
        'classification',
        'designation',
        'average_height',
        'average_lifespan',
        'eye_colors',
        'hair_colors',
        'skin_colors',
        'language',
        'homeworld'
    ];

    public function films()
    {
        return $this->belongsToMany(
            Film::class,
            'films_species',
            'species_id',
            'film_id')->select('films.id');
    }

    public function people()
    {
        return $this->belongsToMany(
            Person::class,
            'people_species',
            'species_id',
            'person_id')->select('people.id');
    }

    public function homeworld(){
        return $this->hasOne(
            Planet::class,
            'id',
            'homeworld'
        );
    }
}
