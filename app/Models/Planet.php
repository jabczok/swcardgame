<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Planet
 *
 * @property int $id
 * @property string $name
 * @property string|null $diameter
 * @property string|null $rotation_period
 * @property string|null $orbital_period
 * @property string|null $gravity
 * @property string|null $population
 * @property string|null $climate
 * @property string|null $terrain
 * @property string|null $surface_water
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Film[] $films
 * @property-read int|null $films_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Person[] $residents
 * @property-read int|null $residents_count
 * @method static \Illuminate\Database\Eloquent\Builder|Planet newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Planet newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Planet query()
 * @method static \Illuminate\Database\Eloquent\Builder|Planet whereClimate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Planet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Planet whereDiameter($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Planet whereGravity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Planet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Planet whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Planet whereOrbitalPeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Planet wherePopulation($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Planet whereRotationPeriod($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Planet whereSurfaceWater($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Planet whereTerrain($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Planet whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Planet extends Model
{
    use HasFactory;

    protected $hidden = ['pivot'];

    protected $fillable = [
        'name',
        'diameter',
        'rotation_period',
        'orbital_period',
        'gravity',
        'population',
        'climate',
        'terrain',
        'surface_water'
    ];

    public function residents()
    {
        return $this->hasMany(
            Person::class,
            'homeworld',
            'id'
        );
    }

    public function films()
    {
        return $this->belongsToMany(
            Film::class,
            'films_planets',
            'planet_id',
            'film_id')->select('films.id');
    }
}
