<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Starship
 *
 * @property int $id
 * @property string $name
 * @property string|null $model
 * @property string|null $starship_class
 * @property string|null $manufacturer
 * @property string|null $cost_in_credits
 * @property string|null $length
 * @property string|null $crew
 * @property string|null $passengers
 * @property string|null $max_atmosphering_speed
 * @property string|null $hyperdrive_rating
 * @property string|null $MGLT
 * @property string|null $cargo_capacity
 * @property string|null $consumables
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $img_url
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Film[] $films
 * @property-read int|null $films_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Person[] $pilots
 * @property-read int|null $pilots_count
 * @method static \Illuminate\Database\Eloquent\Builder|Starship newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Starship newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Starship query()
 * @method static \Illuminate\Database\Eloquent\Builder|Starship whereCargoCapacity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Starship whereConsumables($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Starship whereCostInCredits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Starship whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Starship whereCrew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Starship whereHyperdriveRating($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Starship whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Starship whereImgUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Starship whereLength($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Starship whereMGLT($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Starship whereManufacturer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Starship whereMaxAtmospheringSpeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Starship whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Starship whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Starship wherePassengers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Starship whereStarshipClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Starship whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Starship extends Model
{
    use HasFactory;

    protected $hidden = ['pivot'];

    protected $fillable = [
        'name',
        'model',
        'starship_class',
        'manufacturer',
        'cost_in_credits',
        'length',
        'crew',
        'passengers',
        'max_atmosphering_speed',
        'hyperdrive_rating',
        'MGLT',
        'cargo_capacity',
        'consumables',
        'img_url'
    ];

    public function films()
    {
        return $this->belongsToMany(
            Film::class,
            'films_starships',
            'starship_id',
            'film_id')->select('films.id');
    }

    public function pilots()
    {
        return $this->belongsToMany(
            Person::class,
            'people_starships',
            'starship_id',
            'person_id')->select('people.id');
    }
}
