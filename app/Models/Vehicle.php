<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Vehicle
 *
 * @property int $id
 * @property string $name
 * @property string|null $model
 * @property string|null $vehicle_class
 * @property string|null $manufacturer
 * @property string|null $length
 * @property string|null $cost_in_credits
 * @property string|null $crew
 * @property string|null $passengers
 * @property string|null $max_atmosphering_speed
 * @property string|null $cargo_capacity
 * @property string|null $consumables
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Film[] $films
 * @property-read int|null $films_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Person[] $pilots
 * @property-read int|null $pilots_count
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle query()
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereCargoCapacity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereConsumables($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereCostInCredits($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereCrew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereLength($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereManufacturer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereMaxAtmospheringSpeed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle wherePassengers($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vehicle whereVehicleClass($value)
 * @mixin \Eloquent
 */
class Vehicle extends Model
{
    use HasFactory;

    protected $hidden = ['pivot'];

    protected $fillable = [
        'name',
        'model',
        'vehicle_class',
        'manufacturer',
        'length',
        'cost_in_credits',
        'crew',
        'passengers',
        'max_atmosphering_speed',
        'cargo_capacity',
        'consumables'
    ];

    public function films()
    {
        return $this->belongsToMany(
            Film::class,
            'films_vehicles',
            'vehicle_id',
            'film_id')->select('films.id');
    }

    public function pilots()
    {
        return $this->belongsToMany(
            Person::class,
            'people_vehicles',
            'vehicle_id',
            'person_id')->select('people.id');
    }
}
