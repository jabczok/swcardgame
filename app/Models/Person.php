<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Person
 *
 * @property int $id
 * @property string $name
 * @property string|null $birth_year
 * @property string|null $eye_color
 * @property string|null $gender
 * @property string|null $hair_color
 * @property string|null $height
 * @property string|null $mass
 * @property string|null $skin_color
 * @property \App\Models\Planet|null $homeworld
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $img_url
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Film[] $films
 * @property-read int|null $films_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Species[] $species
 * @property-read int|null $species_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Starship[] $starships
 * @property-read int|null $starships_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Vehicle[] $vehicles
 * @property-read int|null $vehicles_count
 * @method static \Illuminate\Database\Eloquent\Builder|Person newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Person newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Person query()
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereBirthYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereEyeColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereGender($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereHairColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereHomeworld($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereImgUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereMass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereSkinColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Person whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Person extends Model
{
    use HasFactory;

    protected $hidden = ['pivot'];

    protected $fillable = [
        'name',
        'birth_year',
        'eye_color',
        'gender',
        'hair_color',
        'height',
        'skin_color',
        'films',
        'species',
        'starships',
        'vehicles',
        'homeworld',
        'img_url',
        'mass'
    ];

    public function films()
    {
        return $this->belongsToMany(
            Film::class,
            'people_films',
            'person_id',
            'film_id')->select('films.id');
    }

    public function starships()
    {
        return $this->belongsToMany(
            Starship::class,
            'people_starships',
            'person_id',
            'starship_id')->select('starships.id');
    }

    public function species()
    {
        return $this->belongsToMany(
            Species::class,
            'people_species',
            'person_id',
            'species_id')->select('species.id');
    }

    public function vehicles()
    {
        return $this->belongsToMany(
            Vehicle::class,
            'people_vehicles',
            'person_id',
            'vehicle_id')->select('vehicles.id');
    }

    public function homeworld(){
        return $this->hasOne(
            Planet::class,
            'id',
            'homeworld'
        );
    }
}
